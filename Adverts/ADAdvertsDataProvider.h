//
//  ADAdvertsDataProvider.h
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ObjectCompletionBlock)(id object, NSError *error);

@interface ADAdvertsDataProvider : NSObject

+ (instancetype)sharedProvider;

-(void)loadAdvertsWithLimit:(NSInteger)limit andOffset:(NSInteger)offset withCompletion:(ObjectCompletionBlock)completion;

@end
