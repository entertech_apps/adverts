//
//  ADAdvertsViewContorller.m
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import "ADAdvertsViewContorller.h"
#import "ADAdvertsDataProvider.h"
#import "ADAdvertsResponse.h"
#import "ADAdvert.h"
#import "NSNumberFormatter+Additions.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ADAdvertsViewContorller

-(void)viewDidLoad {
    [super viewDidLoad];
    self.tableManager.currentLimit = 10;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableManager fill];
}

- (void)tableManager:(ADTableManager *)tableManager loadWithLimit:(NSInteger)limit offset:(NSInteger)offset {
    
    [[ADAdvertsDataProvider sharedProvider] loadAdvertsWithLimit:limit andOffset:offset withCompletion:^(ADAdvertsResponse *object, NSError *error) {

        if (tableManager.objects == nil || tableManager.objects.count == 0) {
             [SVProgressHUD show];
        }
       
        if (error == nil) {
            [self.tableManager addItems:object.advertItems];
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD showErrorWithStatus:error.description];
        }
    }];
    
}

- (void)tableManager:(ADTableManager *)tableManager didSelectItem:(ADAdvert *)item {
    NSMutableArray *items = [NSMutableArray new];
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    if (item.originImageUrl) {

        [SVProgressHUD show];
        [manager downloadImageWithURL:[NSURL URLWithString:item.originImageUrl]
                          options:0
                         progress:nil
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                [items addObject:image];
                                [items addObject:item.title];
                                [self showActivityViewControllerWithItems:items];
                            }
                            [SVProgressHUD dismiss];
                        }];
    } else {
        [items addObject:item.title];
        [self showActivityViewControllerWithItems:items];
    }
    
}

- (void)showActivityViewControllerWithItems:(NSArray *)items {
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    [self presentViewController:controller animated:YES completion:nil];
}

@end
