//
//  ADAdvertsDataProvider.m
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import "ADAdvertsDataProvider.h"
#import "ADAdvert.h"
#import "ADAdvertsResponse.h"
#import "ADConstants.h"
#import <AFNetworking/AFNetworking.h>
#import <EasyMapping/EasyMapping.h>


static NSString * baseUrl = @"http://do.ngs.ru/api/v1/adverts/?include=uploads,tags&fields=short_images,cost,update_date";

NSString * const kAdberts = @"http://do.ngs.ru/api/v1/adverts";

@implementation ADAdvertsDataProvider

+ (instancetype)sharedProvider {
    
    static dispatch_once_t onceToken;
    static ADAdvertsDataProvider *shared = nil;
    dispatch_once(&onceToken, ^{
        shared = [[[self class] alloc] init];
    });
    return shared;

    
}


-(void)loadAdvertsWithLimit:(NSInteger)limit andOffset:(NSInteger)offset withCompletion:(ObjectCompletionBlock)completion {
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params addEntriesFromDictionary:@{
                                       @"limit"   : @(limit),
                                       @"offset"  : @(offset),
                                    }];

    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    [manager GET:baseUrl parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        ADAdvertsResponse *response = [EKMapper objectFromExternalRepresentation:responseObject
                                                        withMapping:[ADAdvertsResponse objectMapping]];
        
        PERFORM_BLOCK_IF_NOT_NIL(completion, response, nil);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        PERFORM_BLOCK_IF_NOT_NIL(completion, nil, error);
    }];
    
}

@end
