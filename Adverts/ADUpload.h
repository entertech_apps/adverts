//
//  ADUpload.h
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface ADUpload : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *file_name;
@property (nonatomic, copy) NSString *file_extension;
@property (nonatomic, copy) NSString *domain;


- (NSString *)imageUrl;

@end
