//
//  ADUpload.m
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import "ADUpload.h"

@implementation ADUpload

+ (EKObjectMapping *)objectMapping {
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping * _Nonnull mapping) {
        [mapping mapPropertiesFromDictionary:@{
                                               @"id"                : @"uid",
                                               @"file_name"         : @"file_name",
                                               @"file_extension"    : @"file_extension",
                                               @"domain"            : @"domain"
                                               }];
    }];
}

- (NSString *)imageUrl {
    return [NSString stringWithFormat:@"http://%@/%@.%@", self.domain, self.file_name, self.file_extension];
}

@end
