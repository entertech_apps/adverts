//
//  ADTableManager.h
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class ADTableManager;

@protocol ADTableMangerDelegate <NSObject>

-(void)tableManager:(ADTableManager *)tableManager loadWithLimit:(NSInteger)limit offset:(NSInteger)offset;

@optional

- (void)tableManager:(ADTableManager *)tableManager didSelectItem:(id)item;

@end

@interface ADTableManager : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableOrderedSet *objects;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (assign, nonatomic) NSInteger currentLimit;
@property (weak,   nonatomic) IBOutlet id<ADTableMangerDelegate> delegate;

- (void)loadNext;
- (void)fill;
- (void)addItems:(NSArray *)items;

@end


