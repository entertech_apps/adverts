//
//  ADAdvertsResponse.h
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import "ADAdvert.h"

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface ADAdvertsResponse : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray *adverts;
@property (nonatomic, strong) NSDictionary *uploads;
@property (nonatomic, strong) NSDictionary *tags;

-(NSArray *)advertItems;

@end

