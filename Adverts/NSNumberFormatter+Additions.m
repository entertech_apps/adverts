//
//  NSNumberFormatter+Additions.m
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import "NSNumberFormatter+Additions.h"

@implementation NSNumberFormatter (Additions)

+ (instancetype)sharedNumberFormatter {
    static NSNumberFormatter *sharedNumberFormatter = nil ;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedNumberFormatter = [[NSNumberFormatter alloc]init];
    });
    
    return sharedNumberFormatter ;
}

+ (instancetype)numberFormatterForAdvert {
    NSNumberFormatter *numberFormatter = [self sharedNumberFormatter];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"Ru-ru"]];
    return numberFormatter;
}

@end
