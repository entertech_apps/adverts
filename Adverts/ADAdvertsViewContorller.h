//
//  ADAdvertsViewContorller.h
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import "ADTableManager.h"
#import <UIKit/UIKit.h>

@interface ADAdvertsViewContorller : UIViewController <ADTableMangerDelegate>

@property (strong, nonatomic) IBOutlet ADTableManager *tableManager;


@end
