//
//  ADTableManager.m
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import "ADTableManager.h"
#import "ADAdvertsTableViewCell.h"
#import "ADAdvert.h"

#import "UIScrollView+InfiniteScroll.h"

@implementation ADTableManager


#pragma mark - <UITableViewDataSource>

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *cellName = NSStringFromClass([ADAdvertsTableViewCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:cellName bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellName];
    
    ADAdvertsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    [cell fillWithAdvert:self.objects[indexPath.row]];
    return cell;
}

#pragma makr - <UITableViewDateSource>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(tableManager:didSelectItem:)]) {
        [self.delegate tableManager:self didSelectItem:self.objects[indexPath.row]];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)loadNext {
    
    NSInteger offset = 0;
    if (self.objects != nil) {
        offset = self.objects.count;
    }
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(tableManager:loadWithLimit:offset:)]) {
        [self.delegate tableManager:self loadWithLimit:self.currentLimit offset:offset];
    }
    
    [self.tableView addInfiniteScrollWithHandler:^(UITableView * _Nonnull tableView) {
        [self loadNext];
    }];
    
}
- (void)addItems:(NSArray *)items {
    
    if (self.objects == nil) {
        self.objects = [NSMutableOrderedSet new];
    }
    
    [self.objects addObjectsFromArray:items];
    
    [self.tableView reloadData];
    [self.tableView finishInfiniteScroll];
}

- (void)fill {
    [self loadNext];
 
}

@end
