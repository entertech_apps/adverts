//
//  ADAdvertsResponse.m
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import "ADAdvertsResponse.h"
#import "ADAdvert.h"
#import "ADUpload.h"

@implementation ADAdvertsResponse

+ (EKObjectMapping *)objectMapping {
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping * _Nonnull mapping) {
        
        [mapping mapPropertiesFromDictionary:@{
                                               @"linked.uploads"       : @"uploads",
                                               @"linked.tags"          : @"tags"
                                               }];
        
        [mapping hasMany:[ADAdvert class] forKeyPath:@"adverts" forProperty:@"adverts"];

    }];
}

- (NSArray *)advertItems {

    for (ADAdvert *advert in self.adverts) {
        
        if (advert.thumbImageId != nil) {
            ADUpload *upload = [EKMapper objectFromExternalRepresentation:self.uploads[advert.thumbImageId] withMapping:[ADUpload objectMapping]];
            advert.thumbImageUrl = upload.imageUrl;
        }
        
        if (advert.originImageId != nil) {
            ADUpload *upload = [EKMapper objectFromExternalRepresentation:self.uploads[advert.originImageId] withMapping:[ADUpload objectMapping]];
            advert.originImageUrl = upload.imageUrl;
        }
        
        NSMutableArray *tagsTitles = [NSMutableArray new];
        for (NSString *tag in advert.tags) {
            NSString *tagTitle = self.tags[tag][@"title"];
            [tagsTitles addObject:tagTitle];
        }
        advert.tagsTitles = tagsTitles;

    }
    
    return self.adverts;
}

@end
