//
//  ADAdvertsTableViewCell.m
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import "ADAdvertsTableViewCell.h"
#import "NSNumberFormatter+Additions.h"
#import "NSDateFormatter+Additions.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ADAdvertsTableViewCell


-(void)fillWithAdvert:(ADAdvert *)item {
    
    self.titleLabel.text = item.title;
    NSString *typeString;
    if (item.tagsTitles && item.tagsTitles.count > 0) {
        typeString = [NSString stringWithFormat:@"%@",item.tagsTitles[0]];
    }

    NSDateFormatter *dateFormatter = [NSDateFormatter dateFormatterForAdvert];
    NSString *dateString = [dateFormatter stringFromDate:item.update_date];
    
    self.typeLabel.text = [self createInfoStringWith:typeString andString:dateString];
    
    if (item.thumbImageUrl) {
        [self.advertView sd_setImageWithURL:[NSURL URLWithString:item.thumbImageUrl]
                           placeholderImage:[UIImage imageNamed:@"empty_img"]
                                    options:SDWebImageRetryFailed];
    } else {
        self.advertView.image = [UIImage imageNamed:@"empty_img"];
    }
    
    if (item.cost > 0) {
        NSNumberFormatter *numberFormatter = [NSNumberFormatter numberFormatterForAdvert];
        self.priceLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:item.cost]];
    } else {
        self.priceLabel.text = @"цена не указана";
    }
    
    
}

- (NSString *)createInfoStringWith:(NSString *)str1 andString:(NSString *)str2 {
    
    NSMutableString *infoString = [NSMutableString new];
    if (str1 && str2) {
        [infoString appendString:str1];
        [infoString appendString:@" • "];
        [infoString appendString:str2];
    } else if (str1) {
        [infoString appendString:str1];
    } else {
        [infoString appendString:str2];
    }
    
    return infoString;

}

@end
