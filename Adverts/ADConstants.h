//
//  ADConstants.h
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PERFORM_BLOCK_IF_NOT_NIL(block, ...) \
if (block) {\
block(__VA_ARGS__); \
}

@interface ADConstants : NSObject

@end
