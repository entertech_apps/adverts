//
//  ADAdvert.m
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import "ADAdvert.h"
#import "NSDateFormatter+Additions.h"


@implementation ADAdvert

+ (EKObjectMapping *)objectMapping {
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping * _Nonnull mapping) {
        
        [mapping mapKeyPath:@"update_date" toProperty:@"update_date" withDateFormatter:[NSDateFormatter dateFormatterForMapping]];

        [mapping mapPropertiesFromDictionary:@{
                                               @"title"       : @"title",
                                               @"id"          : @"uid",
                                               @"cost"        : @"cost",
                                               @"short_images.main.links.origin" : @"originImageId",
                                               @"short_images.main.links.thumb"  : @"thumbImageId",
                                               @"links.tags"  : @"tags"
                                               }];
        
    }];
    
    
}


@end


