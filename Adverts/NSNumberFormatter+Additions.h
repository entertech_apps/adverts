//
//  NSNumberFormatter+Additions.h
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumberFormatter (Additions)

+ (instancetype)sharedNumberFormatter;
+ (instancetype)numberFormatterForAdvert;

@end
