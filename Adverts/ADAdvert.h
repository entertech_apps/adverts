//
//  ADAdvert.h
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface ADAdvert : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *originImageId;
@property (nonatomic, copy) NSString *thumbImageId;

@property (nonatomic, strong) NSDate *update_date;
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic, readwrite) NSInteger cost;

@property (nonatomic, copy) NSString *originImageUrl;
@property (nonatomic, copy) NSString *thumbImageUrl;
@property (nonatomic, copy) NSArray *tagsTitles;

@end


