//
//  ADAdvertsTableViewCell.h
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import "ADAdvert.h"
#import <UIKit/UIKit.h>

@interface ADAdvertsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *advertView;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

-(void)fillWithAdvert:(ADAdvert* )item;

@end
