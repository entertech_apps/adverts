//
//  NSDateFormatter+Additions.m
//  Adverts
//
//  Created by Yuriy Poluektov on 07.08.16.
//  Copyright © 2016 Yuriy Poluektov. All rights reserved.
//

#import "NSDateFormatter+Additions.h"

@implementation NSDateFormatter (Additions)

+ (instancetype)sharedDateFormatter {
    static NSDateFormatter *sharedDateFormatter = nil ;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDateFormatter = [[NSDateFormatter alloc]init];
    });
    
    return sharedDateFormatter ;
}

+ (instancetype)dateFormatterForAdvert {
    NSDateFormatter *dateFormatter = [self sharedDateFormatter];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];

    return dateFormatter;
}

+ (instancetype)dateFormatterForMapping {
    NSDateFormatter *dateFormatter = [self sharedDateFormatter];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    return dateFormatter;
}

@end
